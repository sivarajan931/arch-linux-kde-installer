#!/bin/bash
# This script install the Desktop, Sets up time and install other required dependencies.
source profile.sh
initial(){
	sed -i 's/^#ParallelDownloads = 5/ParallelDownloads = 5/' /etc/pacman.conf #Enable Parallel downloads.

	echo "Enabling Multilib repo..."

	echo "[multilib]" >>/etc/pacman.conf
	echo "Include = /etc/pacman.d/mirrorlist" >>/etc/pacman.conf
	pacman -Syy
	echo "Installing network drivers..."
	pacman -S --noconfirm --needed networkmanager
	systemctl enable NetworkManager

}
# set time and timezone
# ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
#tzselect
timezone() {
	# Time zone and locale.

	timedatectl set-timezone ${choice_timezone}

	timedatectl set-ntp true
	hwclock --systohc
	echo "KEYMAP=$choice_keymap" >/etc/vconsole.conf
	#set language to en_US
	echo LANG=en_US.UTF-8 >/etc/locale.conf
	echo "en_US.UTF-8 UTF-8" >/etc/locale.gen
	localectl set-locale LANG="en_US.UTF-8" LC_TIME="en_US.UTF-8"
	localectl set-keymap ${KEYMAP}
	locale-gen
}

pacman -Syy

#setting up host name

add_hostname(){
		myhostname=$choice_hostname
		echo "$myhostname" >/etc/hostname
		echo "127.0.0.1    localhost" >>/etc/hosts
		echo "::1          localhost" >>/etc/hosts
		echo "127.0.1.1    $myhostname.localdomain $myhostname" >>/etc/hosts

}


install_grub(){
	pacman -S --noconfirm --needed efibootmgr grub mtools dosfstools os-prober

	echo "Installing GRUB..."
	grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
	status=$?
	if [[ $status -ne 0 ]]; then
		echo "Error occurred. Quitting the installation."
	fi
	reply=$choice_os_prober
	if [[ $reply =~ ^[Yy]$ ]]; then
		echo "GRUB_DISABLE_OS_PROBER=false" >>/etc/default/grub
	fi
	grub-mkconfig -o /boot/grub/grub.cfg

}


gpu_drivers(){
	# GPU drivers.

    choice=$choice_gpu_drivers

	if [[ $choice -eq 1 ]]; then
		pacman -S nvida-dkms nvidia nvidia-settings cuda --noconfirm
		nvidia-xconfig
	elif [[ $choice -eq 2 ]]; then
		pacman -S --noconfirm xf86-video-amdgpu
	elif [[ $choice -eq 3 ]]; then
		pacman -S --noconfirm xf86-video-ati
	elif [[ $choice -eq 4 ]]; then
		pacman -S --noconfirm xf86-video-intel libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-util
	elif [[ $choice -eq 5 ]]; then
		pacman -S --noconfirm xf86-video-vmware
	elif [ $choice -eq 6 ]; then
		pacman -S --noconfirm xf86-video-dummy
	else
		if lspci | grep -E "NVIDIA|GeForce"; then
			pacman -S nvida-dkms nvidia nvidia-settings cuda --noconfirm --needed
		elif lspci | grep -E "Radeon"; then
			pacman -S xf86-video-amdgpu --noconfirm --needed
		elif lspci | grep -E "Integrated Graphics Controller"; then
			pacman -S libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-utils --needed --noconfirm
		elif grep -E "Intel Corporation UHD" <<<${gpu_type}; then
			pacman -S libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-utils lib32-mesa --needed --noconfirm
		fi
	fi
}

install_pulse_audio(){
	pacman -S --noconfirm --needed pulseaudio-alsa alsa-plugins pavucontrol pulseaudio alsa alsa-utils
}

install_pipewire(){
	pacman -S --noconfirm --needed pipewire pipewire-alsa pipewire-pulse
}
install_kde_plasma(){
	echo "Installing KDE plasma desktop environment (Minimal)..."
	pacman -S --noconfirm --needed git xorg sddm plasma konsole kcalc krunner kate firefox vlc ntfs-3g zsh dolphin ark kcalc spectacle packagekit-qt5 partitionmanager wget openssh htop neofetch zsh-completions zsh-syntax-highlighting mtools dosfstools sshfs unrar bash-completion python-pip gwenview okular ublock-origin
	systemctl enable sddm
}

install_xfce(){
	echo "Installing XFCE desktop environment ..."
	sudo pacman -S --noconfirm --needed xorg lightdm openssh neofetch lightdm-gtk-greeter lightdm-gtk-greeter-settings xfce4 xfce4-goodies unrar bash-completion zsh firefox arc-gtk-theme arc-icon-theme vlc gparted mtools dosfstools htop packagekit-qt5 ntfs-3g wget qt zsh-completions zsh-syntax-highlighting lightdm python-pip network-manager-applet ublock-origin
	systemctl enable lightdm

}
install_minimal(){

	echo "Installing just xorg and other file system tools ..."
	sudo pacman -S --noconfirm --needed xorg  openssh neofetch unrar bash-completion zsh iwd mtools dosfstools ntfs-3g wget zsh-completions zsh-syntax-highlighting python-pip


}
add_user(){
	echo "Changing root password"
	passwd

	myusername=$choice_username
	useradd -mG wheel $myusername
	echo "type password for $myusername"
	passwd $myusername
	echo "${myusername} ALL=(ALL) ALL" >> /etc/sudoers.d/${myusername}


}

#Executing function one by one
initial
timezone
add_hostname
add_user
install_grub
install_pulse_audio

if [[ $choice_de -eq 2 ]]; then
	install_xfce
	echo "Base System with Xfce has been installed."

elif [[ $choice_de -eq 3 ]]; then
	install_minimal
else
	install_kde_plasma
	echo "Base System with KDE plasma has been installed."
fi

gpu_drivers

read -p "Do you like to have flatpak package manager? [y/N]" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
	pacman -S flatpak --noconfirm --needed

fi



read -p "Do you like to execute the  post-install.sh script (You can also run the script after reboot)? [y/N]" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
	su $choice_username -s /bin/sh post-install.sh
else
	echo "Thank you for using this script. If there is any bugs with the script, you can open a issue on my Gitlab repository .If you get any errors with the packages that this script installs, please troubleshoot by yourself and or ask your friends or arch linux sub-reddit/forums and notify the fix to me. So that I can add the same fix to help other users."
	echo "Time to say I use Arch BTW!"

fi

