#!/bin/bash
# This script is in progress. Please do not Execute the script.

# manual partition
script_dir=$(pwd)
source profile.sh
partman() {
	echo "Heads up! Root partion is a Linux file system and boot partition is a EFI File System."
	echo "512MB for EFI partion and more than 50 GB For root partition is recommended."
	n=$choice_format
	df=$choice_disk_file

	if [[ $n -eq 1 ]]; then
		fdisk $df
		man_part

	elif [[ $n -eq 2 ]]; then
		cgdisk $df
		man_part

	elif [[ $n -eq 3 ]]; then
		echo "Skipping the partitioning. Now you will be asked to enter the root and boot partition."
		man_clear_boot
		man_clear_root_ext4
	elif [[ $n -eq 4 ]]; then
		echo "Skipping the partitioning I hope you have done it before running the script."
	elif [[ $n -eq 5 ]]; then
		auto_clean_no_swap $df

	else
		echo "Bye"
		exit
	fi

}

man_part() {

	man_clear_root_ext4
	man_clear_boot
	echo "Did you have swap partition [y/N] ? "
	read reply
	if [[ $reply =~ ^[Yy]$ ]]; then
		man_clear_swap
	fi
}
man_clear_boot() {
	while :; do
		fdisk -l
		echo "Enter the boot partition: "
		read boot_partition
		ls ${boot_partition}
		if [[ $? -eq 0 ]]; then
			echo "Are you sure about it [y/N] ? "
			read reply
			if [[ $reply =~ ^[Yy]$ ]]; then
				mkfs.fat -F32 -n BOOT $boot_partition
				mkdir -p /mnt/boot/efi
				echo "Mounting on /mnt/boot/efi"
				if [[ $? -ne 0 ]]; then
					echo "Error While Formatting the disk."
					exit
				fi
				mount ${boot_partition} /mnt/boot/efi

				break
			else
				echo "Try again or Spam ctrl+c to exit."

			fi
		else
			echo "Try again or Spam ctrl+c to exit."

		fi
	done
}

man_clear_root_ext4() {
	while :; do
		fdisk -l
		echo "Enter the root partition: "
		read root_partition
		ls $root_partition
		if [[ $? -eq 0 ]]; then
			echo "Are you sure about it [y/N] ? "
			read reply
			if [[ $reply =~ ^[Yy]$ ]]; then
				mkfs.ext4 $root_partition
				if [[ $? -ne 0 ]]; then
					echo "Error While Formatting the disk."
					exit
				fi
				echo "Mounting on /mnt"
				mount ${root_partition} /mnt

				break
			fi
		else
			echo "Try again or Spam ctrl+c to exit."
		fi
	done
}

man_clear_swap() {
	while :; do
		fdisk -l
		echo "Enter the swap partition: "
		read swap_partition
		ls $swap_partition
		if [[ $? -eq 0 ]]; then
			echo "Are you sure about it [y/N] ? "
			read reply
			if [[ $reply =~ ^[Yy]$ ]]; then
				mkswap $swap_partition
				if [[ $? -ne 0 ]]; then
					echo "Error While Formatting the disk."
					exit
				fi
				swapon

				break
			fi
		else
			echo "Try again or Spam ctrl+c to exit."
		fi
	done
}
auto_clean_no_swap() {
	ls $1 >>/dev/null
	if [[ $? -ne 0 ]]; then
		exit
	fi
	fdisk $1 <<FDISK_CMDS
g
n
1

+512M
t
uefi
n



w
FDISK_CMDS

	if [[ "${1}" =~ "nvme" ]]; then
		boot_partition=${1}p1
		root_partition=${1}p2
	else
		boot_partition=${1}1
		root_partition=${1}2
	fi
	mkfs.fat -F32 $boot_partition
	mkfs.ext4 $root_partition
	if [[ $? -ne 0 ]]; then
		echo "Error While Formatting the disk."
		exit
	fi
	mount ${root_partition} /mnt
	mkdir -p /mnt/boot/efi
	mount ${boot_partition} /mnt/boot/efi
}

create_swap_file() {

	echo "creating Swap File of ${1} MiB"
	dd if=/dev/zero of=/mnt/swapfile bs=1M count=$1 status=progress
	chmod 600 /mnt/swapfile # set permissions.
	chown root /mnt/swapfile
	mkswap /mnt/swapfile
	swapon /mnt/swapfile
	echo "/swapfile	none	swap	sw	0	0" >>/mnt/etc/fstab

}

# Install Linux kernel of your choice
install_kernel() {
	n=$choice_kernel
	if [[ $n -eq 1 ]]; then
		pacstrap /mnt glibc linux-lts dkms linux-lts-headers git vim base base-devel reflector
	elif [[ $n -eq 3 ]]; then
		pacstrap /mnt glibc linux-hardend linux-lts git vim base base-devel dkms linux-hardened-headers linux-lts-headers reflector
	elif [[ $n -eq 4 ]]; then
		pacstrap /mnt glibc linux linux-lts git vim base base-devel dkms linux-headers linux-lts-headers reflector
	else
		pacstrap /mnt glibc linux-zen linux-lts git vim base base-devel dkms linux-lts-headers linux-zen-headers reflector
	fi
	if [[ $? -ne 0 ]]; then
		echo "Error occurred  while installing the system. Relaunch the installer."
		exit
	fi
	proc_type=$(lscpu)
	if grep -E "GenuineIntel" <<<${proc_type}; then
		echo "Installing Intel microcode..."
		pacstrap /mnt intel-ucode
	elif grep -E "AuthenticAMD" <<<${proc_type}; then
		echo "Installing AMD microcode..."
		pacstrap /mnt amd-ucode
	fi
	genfstab -U /mnt >>/mnt/etc/fstab
	# gen_swap_file
	reply=$choice_firmware
	if [[ $reply =~ ^[Yy]$ ]]; then
		pacstrap /mnt linux-firmware linux-firmware-bnx2x linux-firmware-liquidio linux-firmware-marvell linux-firmware-mellanox linux-firmware-nfp linux-firmware-qcom linux-firmware-qlogic linux-firmware-whence
	else
		pacstrap /mnt linux-firmware
	fi
}

gen_swap_file() {

	echo "Are you willing to create a swap file for your PC?[y/N]: "
	read reply
	if [[ $reply =~ ^[Yy]$ ]]; then
		while :; do
			echo "Enter the size in GB: "
			read size
			truesize=$(expr 1024*$size)
			create_swap_file $truesize
		done
	fi

}
check_connection() {
	wget -q --spider http://archlinux.org
	clear
	if [[ $? -ne 0 ]]; then
		echo "You are offline. Please connect to internet using either iwctl or check for internet availability."
		exit
	fi
}

logo() {
	figlet Simple Arch Linux Installer
	neofetch
}

start_up() {
	if [[ -d "/sys/firmware/efi" ]]; then
		logo
		partman
		install_kernel
	else
		echo "This is not a EFI system. If your system supports UEFI"
	fi
}
clear
check_connection
if [[ $UID -ne 0 ]]; then
	echo "You need to be in root in order to execute this script."
	exit
fi
pacman -Syy
pacman -S --noconfirm --needed pacman-contrib terminus-font figlet python-pip arch-install-scripts neofetch
setfont ter-v22b
start_up
mkdir -p /mnt/scripts
cp -R $script_dir/* /mnt/scripts/

echo "Base system has been installed without boot-loader."
arch-chroot /mnt /bin/bash /scripts/install_uefi.sh
