# Simple Arch Linux Installer

A basic shell script to install Arch Linux with KDE plasma desktop/Xfce Desktop environment and other necessary applications for user on top of base installation.


# Key features
- Install Base Arch Linux quickly.
- Auto Format enables you to quickly format the entire partition and setup Arch Linux faster for you.(Allocates 512MB for Boot and rest for root).
- A simple configuration file which can be backed up for future install.
- Installs Arch Linux minimal KDE plasma desktop/ Xfce Desktop Environment.
- Installs necessary Linux applications for the user (User's choice is asked to install applications).
- Sets up AUR helpers and Chaotic AUR.
- Configures grub for dual boot.
- Configures bash and zsh shells.
- You can also install other packages that are needed.


# Contributions
All types of Contributions are welcome. Bugs can be reported by opening an issue for this project.


# How to run
This is just a simple guide for installation of base system for more information check out [arch wiki](https://wiki.archlinux.org).
Be sure to have around 50 GB of hard-disk storage. Make sure you have a stable internet connection. As always know what your doing.

## Part 1: Preliminary Setup
1. Download Arch Linux official iso. (Be sure to verify the ISO)
2. Then using  Rufus (on Windows)/ or ventoy(Linux/Windows) and create a bootable Pendrive. 
3. Reboot your machine.
4. Enter the boot menu and select your USB boot device. Make sure to boot in UEFI mode.
5. Select the first  option.
6. You may now format your disk using fdisk (Not necessary if you are going to format when script is being executed or  if you want to automatically format entire disk and install).

## Part 2: 
1. Check for internet connection.
```ping archlinux.org```
Note:
- for Wi-Fi connections you may need to use iwctl for setting up your internet.
- Check out arch wiki about it.

2. Install git by using the following command:

```pacman -Syy git vim```

3. Now clone this repository using git clone:

```git clone http://gitlab.com/sivarajan931/simple-arch-installer```

4. Create a profile by executing `make-profile.sh`. Answer all the questions carefully.
    ```sh make-profile.sh```

5. Execute the base-installer( If you did not answer yes to execute it):
```sh base-installer.sh```

6. If you want, you can execute `arch-chroot /mnt` and configure your installtion.

7. Reboot your system. If you get SDDM/LightDM login screen and you have sucessfully logged in, Nothing to worry about.

8. Optional Step:
    - You can backup your profile. It is located at `/scripts/profile.sh` (Root permission is required).
    - Delete contents in 
# Limitations

- Currently This script does not support btrfs formatting.
- Only English-US is supported.

# Future work
- Error handling.
- Configruation for ` post-install.sh ` file
- Scripts for fixing common errors like grub install, initramfs not found etc...
- Support for setting up offline installtion by setting up an external media withall the required files for installation.
